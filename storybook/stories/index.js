import React from 'react'
import { Text, View, Animated } from 'react-native'

import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import CenterView from './CenterView'
import Welcome from './Welcome'

import Button from '../../src/components/Button'
import CollapsibleLayout from '../../src/components/CollapsibleLayout'
import Input from '../../src/components/Input'
import Label from '../../src/components/Label'
import TextField from '../../src/components/TextField'

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />)

storiesOf('Button', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('with text', () => (
    <Button onPress={action('clicked-text')}>
      <Text
        style={{
          paddingTop: 15,
          fontSize: 15,
          color: '#fff',
          fontWeight: 'bold',
          textAlign: 'center',
          borderColor: '#fff',
        }}
      >
        {'ver produtos'.toUpperCase()}
      </Text>
    </Button>
  ))
  .add('with some emoji', () => (
    <Button onPress={action('clicked-emoji')}>
      <Text>😀 😎 👍 💯</Text>
    </Button>
  ))

storiesOf('CollapsibleLayout', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('empty', () => <CollapsibleLayout scroll={new Animated.Value(0)} />)
  .add('with scroll', () => (
    <CollapsibleLayout
      scroll={new Animated.Value(0)}
      title={<Text>Home</Text>}
      tabs={[...Array(3)].map((_, i) => <Text key={i}>Tab {i}</Text>)}
    >
      {[...Array(50)].map((_, i) => <Text key={i}>Content {i}</Text>)}
    </CollapsibleLayout>
  ))
  .add('custom color and height', () => (
    <CollapsibleLayout
      height={80}
      color="red"
      scroll={new Animated.Value(0)}
      title={<Text>Home</Text>}
      tabs={[...Array(3)].map((_, i) => <Text key={i}>Tab {i}</Text>)}
    >
      {[...Array(50)].map((_, i) => <Text key={i}>Content {i}</Text>)}
    </CollapsibleLayout>
  ))

storiesOf('TextField', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('with label only', () => <TextField label={<Label>Nome </Label>} onChange={action('TextField change')} />)
  .add('as a form', () => (
    <View style={{ width: 300 }}>
      <TextField label={<Label>Nome </Label>} onChange={action('TextField change')} />
      <TextField label={<Label>E-mail </Label>} onChange={action('TextField change')} />
      <TextField label={<Label>Idade </Label>} onChange={action('TextField change')} />
    </View>
  ))

storiesOf('Input', module)
  .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('disabled', () => <Input value="This field is disabled by default" editable={false} selectTextOnFocus={false} />)
  .add('big', () => <Input size="xl" keyboardType="email-address" placeholder="E-mail" />)
  .add('medium', () => <Input size="md" keyboardType="email-address" placeholder="E-mail" />)
  .add('small', () => <Input size="sm" placeholder="E-mail" />)
  .add('numeric', () => <Input keyboardType="numeric" placeholder="Number" />)
  .add('email', () => <Input keyboardType="email-address" placeholder="E-mail" />)
  .add('phone', () => <Input keyboardType="phone-pad" placeholder="Phone" />)
  .add('max length 10 chars', () => <Input maxLength={10} placeholder="Only 10!" />)
  .add('highlight default style', () => <Input />)
  .add('highlight custom style', () => <Input borderColors={{ onFocus: 'red', onBlur: 'green' }} />)
  .add('with placeholder', () => <Input placeholder="E-mail" />)
  .add('handle event', () => <Input onChange={action('onChange')} />)
  .add('with empty value', () => <Input />)
