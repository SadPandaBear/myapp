import React from 'react'
import { AppRegistry } from 'react-native'

import App from './src/scenes/App'
import Store from './src/store'

const connectedApp = () => (
  <Store>
    <App />
  </Store>
)

AppRegistry.registerComponent('MyApp', () => connectedApp)
