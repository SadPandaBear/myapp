import React from 'react'
import { Text } from 'react-native'

import renderer from 'react-test-renderer'

import Button from '../components/Button'

describe('Button', () => {
  const Component = props => (
    <Button {...props}>
      <Text>OK</Text>
    </Button>
  )

  it('should render Button', () => {
    const tree = renderer.create(<Component />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
