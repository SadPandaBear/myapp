import React from 'react'
import { TextInput } from 'react-native'

import PropTypes from 'prop-types'

import { getAndroidTextInputBorder, getStylesBySize } from './helper'
import { $black, $grey } from '../../utils/colors/ecommerce'

class Input extends React.Component {
  state = {
    hasFocus: false,
  }

  render() {
    const { containerStyle, size, value, borderColors, onChange, ...props } = this.props
    const { borderStyle, underlineColorAndroid } = getAndroidTextInputBorder(this.state.hasFocus, value, borderColors)

    return (
      <TextInput
        style={[getStylesBySize(size), borderStyle, containerStyle]}
        placeholderTextColor={$black}
        underlineColorAndroid={underlineColorAndroid}
        onChangeText={onChange}
        onBlur={() => this.setState({ hasFocus: false })}
        onFocus={() => this.setState({ hasFocus: true })}
        {...props}
      />
    )
  }
}

Input.defaultProps = {
  onChange: () => {},
  size: 'md',
  borderColors: {
    onFocus: $grey,
    onBlur: $black,
  },
  containerStyle: null,
}

Input.propTypes = {
  size: PropTypes.oneOf(['md', 'xl', 'sm']),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  borderColors: PropTypes.objectOf(PropTypes.string),
  onChange: PropTypes.func,
  containerStyle: PropTypes.object,
}

export default Input
