import React from 'react'
import { TextInput } from 'react-native'

import PropTypes from 'prop-types'

import { getTextInputBorder, getStylesBySize } from './helper'
import { $black, $grey } from '../../utils/colors/ecommerce'

class Input extends React.Component {
  state = {
    hasFocus: false,
  }

  render() {
    const { size, value, borderColors, onChange, ...props } = this.props
    const borderStyle = getTextInputBorder(this.state.hasFocus, value, borderColors)

    return (
      <TextInput
        style={[getStylesBySize(size), borderStyle, containerStyle]}
        placeholderTextColor={$black}
        underlineColorAndroid="transparent"
        onChangeText={onChange}
        onBlur={() => this.setState({ hasFocus: false })}
        onFocus={() => this.setState({ hasFocus: true })}
        {...props}
      />
    )
  }
}

Input.defaultProps = {
  onChange: () => {},
  size: 'md',
  borderColors: {
    onFocus: $grey,
    onBlur: $black,
  },
  containerStyle: null,
}

Input.propTypes = {
  size: PropTypes.oneOf(['md', 'xl', 'sm']),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  borderColors: PropTypes.objectOf(PropTypes.string),
  onChange: PropTypes.func,
  containerStyle: PropTypes.object,
}

export default Input
