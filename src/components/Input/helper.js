import { StyleSheet } from 'react-native'

const sizeStyles = StyleSheet.create({
  xl: {
    width: 280,
    maxWidth: 280,
    minWidth: 180,
  },
  md: {
    width: 230,
    maxWidth: 230,
    minWidth: 180,
  },
  sm: {
    width: 210,
    maxWidth: 210,
    minWidth: 180,
  },
})

const highlightStyle = hasFocus => fn => (colorTrue, colorFalse) => fn(hasFocus ? colorTrue : colorFalse)

export const getTextInputBorder = (hasFocus, hasValue, colors) => {
  const { onFocus, onBlur } = colors
  const border = color => ({
    color,
    padding: 0,
    borderBottomWidth: 0.5,
    borderColor: color,
  })
  return highlightStyle(hasFocus || hasValue)(border)(onFocus, onBlur)
}

export const getAndroidTextInputBorder = (hasFocus, hasValue, colors) => {
  const { onFocus, onBlur } = colors
  const border = color => ({ borderStyle: { color: color }, underlineColorAndroid: color })
  return highlightStyle(hasFocus || hasValue)(border)(onFocus, onBlur)
}

export const getStylesBySize = size => sizeStyles[size]
