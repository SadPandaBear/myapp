import React from 'react'
import { Animated, View } from 'react-native'

import { $primary } from '../../utils/colors/ecommerce'

import PropTypes from 'prop-types'

const NAVBAR_HEIGHT = 56
const COLOR = $primary

/* scroll as `new Animated.Value(0)`
 */
const CollapsibleLayout = ({ scroll, height, color, ...props }) => {
  const y = Animated.multiply(Animated.diffClamp(scroll, 0, height), -1)
  const tabY = Animated.add(scroll, y)
  return (
    <View
      style={{
        alignSelf: 'stretch',
      }}
    >
      <Animated.View
        style={{
          width: '100%',
          height: height,
          position: 'absolute',
          transform: [
            {
              translateY: y,
            },
          ],
          elevation: 0,
          flex: 1,
          zIndex: 1,
          backgroundColor: color,
        }}
      >
        {props.title}
      </Animated.View>
      <Animated.ScrollView
        scrollEventThrottle={1}
        bounces={false}
        showsVerticalScrollIndicator={false}
        style={{ zIndex: 0, height: '100%', elevation: -1 }}
        contentContainerStyle={{ paddingTop: height }}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scroll } } }], { useNativeDriver: true })}
        overScrollMode="never"
      >
        <Animated.View
          style={[
            {
              transform: [{ translateY: tabY }],
              zIndex: 1,
              width: '100%',
              backgroundColor: color,
            },
          ]}
        >
          {props.tabs}
        </Animated.View>
        {props.children}
      </Animated.ScrollView>
    </View>
  )
}

CollapsibleLayout.defaultProps = {
  height: NAVBAR_HEIGHT,
  color: COLOR,
  children: null,
  tabs: null,
  title: null,
}

CollapsibleLayout.propTypes = {
  scroll: PropTypes.instanceOf(Animated.Value).isRequired,
  height: PropTypes.number,
  color: PropTypes.string,
  children: PropTypes.node,
  tabs: PropTypes.node,
  title: PropTypes.node,
}

export default CollapsibleLayout
