import React from 'react'
import { Text } from 'react-native'

import PropTypes from 'prop-types'

const Label = ({ containerStyle, children, ...props }) => (
  <Text style={[containerStyle]} {...props}>
    {children}
  </Text>
)

Label.defaultProps = {
  children: null,
  containerStyle: null,
}

Label.propTypes = {
  children: PropTypes.node,
  containerStyle: PropTypes.object,
}

export default Label
