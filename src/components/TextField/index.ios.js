import React from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import Input from '../Input'

const TextField = ({ onChange, left, right, label }) => (
  <View
    style={{
      flexWrap: 'wrap',
      flexDirection: 'row',
      alignItems: 'flex-start',
    }}
  >
    {React.cloneElement(label, {
      containerStyle: {
        paddingTop: 15,
        margin: 0,
        width: '20%',
        flexWrap: 'wrap',
      },
    })}
    <Input containerStyle={{ margin: 0, width: '80%' }} onChange={onChange} />
    {left}
    {right}
  </View>
)

TextField.defaultProps = {
  onChange: () => {},
  left: null,
  right: null,
  label: null,
}

TextField.propTypes = {
  onChange: PropTypes.func,
  left: PropTypes.node,
  right: PropTypes.node,
  label: PropTypes.node,
}

export default TextField
