import React from 'react'
import PropTypes from 'prop-types'
import { TouchableNativeFeedback, View } from 'react-native'
import { $primary } from '../../utils/colors/ecommerce'

const Button = ({ children, containerStyle, onPress, ...props }) => (
  <TouchableNativeFeedback background={TouchableNativeFeedback.SelectableBackground()} onPress={onPress} {...props}>
    <View
      style={[
        {
          elevation: 5,
          backgroundColor: $primary,
          width: 254,
          height: 50,
          borderRadius: 5,
        },
        containerStyle,
      ]}
    >
      {children}
    </View>
  </TouchableNativeFeedback>
)

Button.defaultProps = {
  children: null,
  onPress: () => {},
  containerStyle: null,
}

Button.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  containerStyle: PropTypes.object,
}

export default Button
