import { connect } from 'react-redux'
import bindActionCreators from '../utils/action-binder'
import { addPeople, deletePeople } from '../store/people'
import Button from '../components/Button'
import Input from '../components/Input'

import React from 'react'
import { Text, View } from 'react-native'

class App extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  state = {
    name: '',
  }

  handleChange(text) {
    this.setState({
      name: text,
    })
  }

  handleClick() {
    const { name } = this.state
    if (name) {
      this.props.addPeople(this.state.name)
      this.setState({
        name: '',
      })
    }
  }

  render() {
    return (
      <View>
        {this.props.people.map((p, i) => (
          <View key={i}>
            <Text>{p.name}</Text>
            <Button onPress={() => this.props.deletePeople(i)}>
              <Text>Delete Person</Text>
            </Button>
          </View>
        ))}

        <Input value={this.state.name} onChange={this.handleChange} />
        <Button onPress={this.handleClick}>
          <Text>Add Person</Text>
        </Button>
      </View>
    )
  }
}

export default connect(
  state => ({
    people: state.people.people,
  }),
  bindActionCreators({
    addPeople,
    deletePeople,
  }),
)(App)
