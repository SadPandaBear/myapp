export const ADD_PEOPLE = 'ADD_PEOPLE'
export const DELETE_PEOPLE = 'DELETE_PEOPLE'

export const addPeople = name => ({ type: ADD_PEOPLE, name })
export const deletePeople = index => ({ type: DELETE_PEOPLE, index })
