import { ADD_PEOPLE, DELETE_PEOPLE } from './actions'

const initialState = {
  people: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_PEOPLE:
      return {
        ...state,
        people: [...state.people, { name: action.name }],
      }
    case DELETE_PEOPLE:
      return {
        ...state,
        people: state.people.filter((_, i) => i !== action.index),
      }
    default:
      return state
  }
}
