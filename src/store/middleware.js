import { applyMiddleware, compose } from 'redux'

import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'

const sagaMiddleware = createSagaMiddleware()

let middlewares = []
if (__DEV__ === true) {
  middlewares.push(logger)
}

const middleware = applyMiddleware(sagaMiddleware, ...middlewares)

export { middleware, sagaMiddleware }
